#include "../include/imagemFiltroCinza.hpp"

#include <cstring>
#include <iostream>
#include <string>

#define MAX_NAME 256
#define MAX 1000

using namespace std;

ImagemFiltroCinza::ImagemFiltroCinza(){

}

ImagemFiltroCinza::ImagemFiltroCinza(char nome[MAX_NAME],string codigo,int largura, int altura, int escala,pixel** pontos):Imagem(nome,codigo,largura,altura,escala,pontos){

}

ImagemFiltroCinza::~ImagemFiltroCinza(){

}

void ImagemFiltroCinza::filtro(){
	int largura, altura;
	pixel** pontos;
	largura = getLargura();
	altura = getAltura();
	pontos = getPontos();

  for (int i = 0; i < altura; i++){
      for (int j = 0; j < largura; j++){
          pontos[i][j].r = (int) ((0.299 * pontos[i][j].r) + (0.587 * pontos[i][j].g) + (0.144 * pontos[i][j].b));
          pontos[i][j].g = pontos[i][j].r;
          pontos[i][j].b = pontos[i][j].r;

          if (pontos[i][j].r > 255){
            pontos[i][j].r = 255;
            pontos[i][j].g = 255;
            pontos[i][j].b = 255;

          }

      }
  }
}
