#ifndef IMAGEMFILTROCINZA_HPP
#define IMAGEMFILTROCINZA_HPP
#include <string>
#include "imagem.hpp"

using namespace std;

class ImagemFiltroCinza:public Imagem{

public:
  ImagemFiltroCinza();
  ImagemFiltroCinza(char nome[MAX_NAME],string codigo,int largura,int altura,int escala, pixel** pontos);
  ~ImagemFiltroCinza();

  void filtro();
};
#endif
