#include "../include/imagemFiltroEspelho.hpp"

#include <cstring>
#include <iostream>
#include <string>

#define MAX_NAME 256
#define MAX 1000

using namespace std;

ImagemFiltroEspelho::ImagemFiltroEspelho(){

}

ImagemFiltroEspelho::ImagemFiltroEspelho(char nome[MAX_NAME],string codigo,int largura, int altura, int escala,pixel** pontos):Imagem(nome,codigo,largura,altura,escala,pontos){

}

ImagemFiltroEspelho::~ImagemFiltroEspelho(){

}

void ImagemFiltroEspelho::filtro(){
	int largura, altura;
	pixel** pontos;
	largura = getLargura();
	altura = getAltura();
	pontos = getPontos();

  pixel img[altura][largura];

	 for (int i = 0; i < altura; i++) {
			 for (int j = 0; j < largura; j++) {
					 img[i][j].r = pontos[i][largura - j].r;
					 img[i][j].g = pontos[i][largura - j].g;
					 img[i][j].b = pontos[i][largura - j].b;
			 }
	 }

	 for (int i = 0; i < altura; i++) {
			 for (int j = 0; j < largura; j++) {
					 pontos[i][j].r = img[i][j].r;
					 pontos[i][j].g = img[i][j].g;
					 pontos[i][j].b = img[i][j].b;
			 }
	 }
}
