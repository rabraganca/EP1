#ifndef IMAGEMFILTROESPELHO_HPP
#define IMAGEMFILTROESPELHO_HPP
#include <string>
#include "imagem.hpp"

using namespace std;

class ImagemFiltroEspelho:public Imagem{

public:
  ImagemFiltroEspelho();
  ImagemFiltroEspelho(char nome[MAX_NAME],string codigo,int largura,int altura,int escala, pixel** pontos);
  ~ImagemFiltroEspelho();

  void filtro();
};
#endif
