#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>

#define MAX 1000
#define MAX_NAME 256

using namespace std;

struct pixel{
	int r,g,b;
};

class Imagem{

private:
	char nome[MAX_NAME];
	string codigo;
	int largura;
	int altura;
	int escala;
	pixel** pontos;

public:
	Imagem();
	Imagem(char nome[MAX_NAME],string codigo,int largura,int altura,int escala, pixel** pontos);
	~Imagem();

	char* getNome();
	void setNome(char *nome);
	string getCodigo();
	void setCodigo(string codigo);
	int getLargura();
	void setLargura(int largura);
	int getAltura();
	void setAltura(int altura);
	int getEscala();
	void setEscala(int escala);
	pixel** getPontos();
	void setPontos(pixel** pontos);
	void filtro();
};
#endif
