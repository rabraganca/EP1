#include "../include/visao.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <fstream>


#define MAX_NAME 256
#define MAX 1000

using namespace std;



char* Visao::lerNomeEntrada(){
	char* nome =(char*)malloc(sizeof(char)*MAX_NAME);
	cout << "\nInsira o nome do arquivo de entrada: " << endl;
	cin >> nome;
	return nome;
}

char* Visao::lerNomeSaida(){
	char* nome =(char*)malloc(sizeof(char)*MAX_NAME);
	cout << "\nInsira o nome do novo arquivo a ser salvo: " << endl;
	cin >> nome;
	return nome;
}

void Visao::msgErro(){
	cerr << "Erro. Arquivo nao encontrado. Certifique-se de acrescentar .ppm ao nome do arquivo." << endl;
}

ImagemFiltroCinza* Visao::criaImagemFiltroCinza(char nomeEntrada[MAX_NAME]){
	ImagemFiltroCinza* objeto;
	ifstream inFile;
	ofstream outFile;
	char nomeAux[MAX_NAME];
	int largura,altura,escala;
	pixel** pontos;
	string codigo;

	pontos = new pixel* [MAX];
	for(int i=0 ; i<MAX ; i++){
		pontos[i] = new pixel[MAX];
	}

	strcpy(nomeAux, "doc/");
	strcat(nomeAux, nomeEntrada);
	inFile.open(nomeAux);
	if(inFile.fail()){
		msgErro();
		exit(1);
	}else{
		inFile >> codigo >> largura >> altura >> escala;
		for(int i=0 ; i<altura ; i++){
			for(int j=0 ; j<largura ; j++){
				inFile >> pontos[i][j].r >> pontos[i][j].g >> pontos[i][j].b;
			}
		}
		inFile.close();
		 objeto = new ImagemFiltroCinza(nomeAux,codigo,largura,altura,escala,pontos);
		 return objeto;
	}
}

ImagemFiltroRelevo* Visao::criaImagemFiltroRelevo(char nomeEntrada[MAX_NAME]){
	ImagemFiltroRelevo* objeto;
	ifstream inFile;
	ofstream outFile;
	char nomeAux[MAX_NAME];
	int largura,altura,escala;
	pixel** pontos;
	string codigo;

	pontos = new pixel* [MAX];
	for(int i=0 ; i<MAX ; i++){
		pontos[i] = new pixel[MAX];
	}

	strcpy(nomeAux, "doc/");
	strcat(nomeAux, nomeEntrada);
	inFile.open(nomeAux);
	if(inFile.fail()){
		msgErro();
		exit(1);
	}else{
		inFile >> codigo >> largura >> altura >> escala;
		for(int i=0 ; i<altura ; i++){
			for(int j=0 ; j<largura ; j++){
				inFile >> pontos[i][j].r >> pontos[i][j].g >> pontos[i][j].b;
			}
		}
		inFile.close();
		 objeto = new ImagemFiltroRelevo(nomeAux,codigo,largura,altura,escala,pontos);
		 return objeto;
	}
}

ImagemFiltroEspelho* Visao::criaImagemFiltroEspelho(char nomeEntrada[MAX_NAME]){
	ImagemFiltroEspelho* objeto;
	ifstream inFile;
	ofstream outFile;
	char nomeAux[MAX_NAME];
	int largura,altura,escala;
	pixel** pontos;
	string codigo;

	pontos = new pixel* [MAX];
	for(int i=0 ; i<MAX ; i++){
		pontos[i] = new pixel[MAX];
	}

	strcpy(nomeAux, "doc/");
	strcat(nomeAux, nomeEntrada);
	inFile.open(nomeAux);
	if(inFile.fail()){
		msgErro();
		exit(1);
	}else{
		inFile >> codigo >> largura >> altura >> escala;
		for(int i=0 ; i<altura ; i++){
			for(int j=0 ; j<largura ; j++){
				inFile >> pontos[i][j].r >> pontos[i][j].g >> pontos[i][j].b;
			}
		}
		inFile.close();
		 objeto = new ImagemFiltroEspelho(nomeAux,codigo,largura,altura,escala,pontos);
		 return objeto;
	}
}

	int Visao::validaNome(char nomeEntrada[MAX_NAME]){
		char nomeAux[MAX_NAME];
		ifstream inFile;
		strcpy(nomeAux, "doc/");
		strcat(nomeAux, nomeEntrada);
		inFile.open(nomeAux);
		if(inFile.fail()){
			msgErro();
			return 0;
		}else{
			inFile.close();
			return 1;
		}
	}

void Visao::escreveObjeto(char nomeSaida[MAX_NAME],Imagem* imagem){
	ofstream outFile;
	char nomeAux[MAX_NAME];
	pixel** pontos ;
	pontos = imagem->getPontos();
	strcpy(nomeAux, "doc/");
	strcat(nomeAux, nomeSaida);
	outFile.open(nomeAux);
	outFile << imagem->getCodigo() << endl;
	outFile << imagem->getLargura() << " " << imagem->getAltura() << endl;
	outFile << imagem->getEscala() << endl;
	for(int i=0 ; i<imagem->getAltura() ; i++){
		for(int j=0 ; j<imagem->getLargura() ; j++){
			outFile << pontos[i][j].r << " " <<  pontos[i][j].g << " " << pontos[i][j].b << endl;
		}
	}
	outFile.close();
	delete(imagem);
	cout << "\nArquivo criado com sucesso." << endl;
}
