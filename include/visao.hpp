#ifndef VISAO_HPP
#define VISAO_HPP

#include <fstream>
#include "imagem.hpp"
#include "imagemFiltroCinza.hpp"
#include "imagemFiltroRelevo.hpp"
#include "imagemFiltroEspelho.hpp"

#define MAX_NAME 256

using namespace std;


class Visao{
public:
	char* lerNomeEntrada();
	char* lerNomeSaida();
	void msgErro();
	ImagemFiltroCinza* criaImagemFiltroCinza(char nomeEntrada[MAX_NAME]);
	ImagemFiltroRelevo* criaImagemFiltroRelevo(char nomeEntrada[MAX_NAME]);
	ImagemFiltroEspelho* criaImagemFiltroEspelho(char nomeEntrada[MAX_NAME]);
	void escreveObjeto(char nomeSaida[MAX_NAME],Imagem* imagem);
	int validaNome(char nomeEntrada[MAX_NAME]);
};
#endif
