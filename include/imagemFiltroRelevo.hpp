#ifndef IMAGEMFILTRORELEVO_HPP
#define IMAGEMFILTRORELEVO_HPP
#include <string>
#include "imagem.hpp"

using namespace std;

class ImagemFiltroRelevo:public Imagem{

public:
  ImagemFiltroRelevo();
  ImagemFiltroRelevo(char nome[MAX_NAME],string codigo,int largura,int altura,int escala, pixel** pontos);
  ~ImagemFiltroRelevo();

  void filtro();
};
#endif
