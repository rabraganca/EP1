#include "../include/imagemFiltroRelevo.hpp"

#include <cstring>
#include <iostream>
#include <string>

#define MAX_NAME 256
#define MAX 1000

using namespace std;

ImagemFiltroRelevo::ImagemFiltroRelevo(){
}

ImagemFiltroRelevo::ImagemFiltroRelevo(char nome[MAX_NAME],string codigo,int largura, int altura, int escala,pixel** pontos):Imagem(nome,codigo,largura,altura,escala,pontos){

}

ImagemFiltroRelevo::~ImagemFiltroRelevo(){

}

void ImagemFiltroRelevo::filtro(){
	int largura, altura;
	pixel** pontos;
	largura = getLargura();
	altura = getAltura();
	pontos = getPontos();

  pixel img[altura][largura];
	 for (int i = 1; i < altura - 1; i++){
			 for (int j = 0; j < largura; j++){
					 img[i][j].r = pontos[i + 1][j].r - pontos[i -1][j].r;
					 img[i][j].g = pontos[i + 1][j].b - pontos[i -1][j].b;
					 img[i][j].b = pontos[i + 1][j].b - pontos[i -1][j].b;

					 if (img[i][j].r < 0)
							 img[i][j].r = 0;

					 if (img[i][j].g < 0)
							 img[i][j].g = 0;

					 if (img[i][j].b < 0)
							 img[i][j].b = 0;
			 }
	 }

	 for (int i = 1; i < altura - 1; i++){
			 for(int j = 0; j < largura; j++){

							 pontos[i][j].r = img[i][j].r + 128;
							 pontos[i][j].g = img[i][j].g + 128;
							 pontos[i][j].b = img[i][j].b + 128;

							 if (img[i][j].r > 255)
							 img[i][j].r = 255;

							 if (img[i][j].g > 255)
							 img[i][j].g = 255;

							 if (img[i][j].b > 255)
							 img[i][j].b = 255;
			 }
	 }
}
