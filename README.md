# EP1 - OO (UnB - Gama)

Este projeto consiste em um programa em C++ capaz de aplicar filtros em imagens de formato `.ppm`.

## Neste arquivo deve conter as instruções de execução e descrição do projeto.

## EP1 - C++

Este programa permite ao usuario selecionar uma imagem em formato ppm, aplicar um filtro (dentre os disponiveis) e salvar a nova imagem sob o nome escolhido em um novo arquivo.

Este programa realiza os seguintes passos:

* 1 - Leitura do nome do arquivo.
* 2 - Validacao do nome do arquivo.
* 3 - Leitura do conteudo do arquivo.
* 4 - Criacao do objeto referente ao filtro escolhido.
* 5 - Leitura do nome do novo arquivo.
* 6 - Gravacao do novo arquivo na pasta doc.

O usuario pode executar as seguintes acoes:

* 1 - Escolha de opcao no menu.
* 2 - Escolha do arquivo de entrada atraves do nome.
* 3 - Escolha do filtro a ser aplicado
* 4 - Escolha do nome de saida do arquivo a ser gravado.



### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**
