#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <cstring>
#include "../include/visao.hpp"
#include "../include/imagem.hpp"
#include "../include/imagemFiltroCinza.hpp"
#include "../include/imagemFiltroEspelho.hpp"
#include "../include/imagemFiltroRelevo.hpp"

#define MAX_NAME 256
#define MAX 1000


using namespace std;


int main(){
	char nomeEntrada[MAX_NAME],nomeSaida[MAX_NAME];
	int opcaoMenu,opcaoFiltro,valida;
	Visao visao;
	ImagemFiltroCinza* imagemCinza;
	ImagemFiltroRelevo* imagemRelevo;
	ImagemFiltroEspelho* imagemEspelho;

	do{
		cout << "\n\nEscolha uma opcao: " << endl;
		cout << "\n1- Aplicar filtro a uma imagem\n2- Sair\n" << endl;
		cin >> opcaoMenu;

		switch(opcaoMenu){
			case 1:
				do{
					strcpy(nomeEntrada,visao.lerNomeEntrada());
					valida = visao.validaNome(nomeEntrada);
				}while(valida == 0);
				cout << "\nEscolha um filto:\n1-Filtro Cinza\n2-Filto alto relevo\n3-Filtro espelhar imagem\n" << endl;
				cin >> opcaoFiltro;
				switch(opcaoFiltro){
					case 1:
						imagemCinza = visao.criaImagemFiltroCinza(nomeEntrada);
						imagemCinza->filtro();
						strcpy(nomeSaida,visao.lerNomeSaida());
						visao.escreveObjeto(nomeSaida,imagemCinza);
						break;
					case 2:
						imagemRelevo = visao.criaImagemFiltroRelevo(nomeEntrada);
						imagemRelevo->filtro();
						strcpy(nomeSaida,visao.lerNomeSaida());
						visao.escreveObjeto(nomeSaida,imagemRelevo);
						break;
					case 3:
						imagemEspelho = visao.criaImagemFiltroEspelho(nomeEntrada);
						imagemEspelho->filtro();
						strcpy(nomeSaida,visao.lerNomeSaida());
						visao.escreveObjeto(nomeSaida,imagemEspelho);
						break;
				}
				break;
			case 2:
				cout << "\nFinalizando o programa.\n\n" << endl;
				exit(1);
		}
	}while(opcaoMenu != 2);

	return 0;
}
