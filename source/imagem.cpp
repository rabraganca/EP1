#include "../include/imagem.hpp"


#include <cstring>
#include <iostream>
#include <string>

#define MAX_NAME 256
#define MAX 1000

using namespace std;

Imagem::Imagem(){
	strcpy(nome,"");
	codigo = " ";
	largura = 0;
	altura = 0;
	escala = 0;
}


Imagem::Imagem(char nome[MAX_NAME],string codigo,int largura, int altura, int escala,pixel** pontos){
	strcpy(this->nome, nome);
	this->codigo = codigo;
	this->largura = largura;
	this->altura = altura;
	this->escala = escala;
	this->pontos = pontos;
}

Imagem::~Imagem(){

}

char* Imagem::getNome(){
	return this->nome;
}
void Imagem::setNome(char* nome){
	strcpy(this->nome, nome);
}

string Imagem::getCodigo(){
	return codigo;
}
void Imagem::setCodigo(string codigo){
	this->codigo = codigo;
}

int Imagem::getLargura(){
	return largura;
}
void Imagem::setLargura(int largura){
	this->largura = largura;
}

int Imagem::getAltura(){
	return altura;
}
void Imagem::setAltura(int altura){
	this->altura = altura;
}

int Imagem::getEscala(){
	return escala;
}
void Imagem::setEscala(int escala){
	this->escala = escala;
}

pixel** Imagem::getPontos(){
	return this->pontos;
}
void Imagem::setPontos(pixel** pontos){
	this->pontos = pontos;
}

void Imagem::filtro(){

}
// void Imagem::filtroCinzaImagem(){
//     for (int i = 0; i < this->altura; i++){
//         for (int j = 0; j < this->largura; j++){
//             this->pontos[i][j].r = (int) ((0.299 * this->pontos[i][j].r) + (0.587 * this->pontos[i][j].g) + (0.144 * this->pontos[i][j].b));
//           	this->pontos[i][j].g = this->pontos[i][j].r;
//           	this->pontos[i][j].b = this->pontos[i][j].r;
//
//             if (this->pontos[i][j].r > 255){
//               this->pontos[i][j].r = 255;
//               this->pontos[i][j].g = 255;
//               this->pontos[i][j].b = 255;
//
//             }
//
//         }
//     }
// } // FONTE: https://www.vivaolinux.com.br/artigo/Manipulacao-de-imagens-no-formato-PPM?pagina=3


// void Imagem::autoRelevoImagem(){
// 	pixel img[this->altura][this->largura];
// 	 for (int i = 1; i < this->altura - 1; i++){
// 			 for (int j = 0; j < this->largura; j++){
// 					 img[i][j].r = pontos[i + 1][j].r - pontos[i -1][j].r;
// 					 img[i][j].g = pontos[i + 1][j].b - pontos[i -1][j].b;
// 					 img[i][j].b = pontos[i + 1][j].b - pontos[i -1][j].b;
//
// 					 if (img[i][j].r < 0)
// 							 img[i][j].r = 0;
//
// 					 if (img[i][j].g < 0)
// 							 img[i][j].g = 0;
//
// 					 if (img[i][j].b < 0)
// 							 img[i][j].b = 0;
// 			 }
// 	 }
//
// 	 for (int i = 1; i < this->altura - 1; i++){
// 			 for(int j = 0; j < this->largura; j++){
//
// 							 pontos[i][j].r = img[i][j].r + 128;
// 							 pontos[i][j].g = img[i][j].g + 128;
// 							 pontos[i][j].b = img[i][j].b + 128;
//
// 							 if (img[i][j].r > 255)
// 							 img[i][j].r = 255;
//
// 							 if (img[i][j].g > 255)
// 							 img[i][j].g = 255;
//
// 							 if (img[i][j].b > 255)
// 							 img[i][j].b = 255;
// 			 }
// 	 }
// }

// void Imagem::espelharImagem(){
// 	pixel img[this->altura][this->largura];
//
// 	 for (int i = 0; i < this->altura; i++) {
// 			 for (int j = 0; j < this->largura; j++) {
// 					 img[i][j].r = pontos[i][this->largura - j].r;
// 					 img[i][j].g = pontos[i][this->largura - j].g;
// 					 img[i][j].b = pontos[i][this->largura - j].b;
// 			 }
// 	 }
//
// 	 for (int i = 0; i < this->altura; i++) {
// 			 for (int j = 0; j < this->largura; j++) {
// 					 pontos[i][j].r = img[i][j].r;
// 					 pontos[i][j].g = img[i][j].g;
// 					 pontos[i][j].b = img[i][j].b;
// 			 }
// 	 }
// }
